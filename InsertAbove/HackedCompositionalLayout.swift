/*
 * HackedCompositionalLayout.swift
 * InsertAbove
 *
 * Created by François Lamboley on 2022/03/21.
 */

import Foundation
import UIKit

import XibLoc



class HackedCompositionalLayout : NSObject {
	
	static var height: CGFloat?
	static var offset: CGFloat?
	
	var actualLayout: UICollectionViewCompositionalLayout
	
	init(actualLayout: UICollectionViewCompositionalLayout) {
		self.actualLayout = actualLayout
		super.init()
	}
	
	override class func forwardingTarget(for aSelector: Selector!) -> Any? {
//		print("+ \(aSelector!)")
		return UICollectionViewCompositionalLayout.self
	}
	
	override func forwardingTarget(for aSelector: Selector!) -> Any? {
//		let selectorStr = "\(aSelector!)"
//		print("\(selectorStr.hasPrefix("_") ? "   " : "")- \(selectorStr)")
		return actualLayout
	}
	
	@objc func collectionViewContentSize() -> CGSize {
		let res = actualLayout.collectionViewContentSize
//		print("- collectionViewContentSize")
//		print("   -> \(res)")
		return res
	}
	
//	@objc func layoutAttributesForElementsInRect(_ rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
//		let delta: CGFloat
//		if let height = Self.height {
//			delta = 0*(collectionViewContentSize().height - height)
//		} else {
//			delta = 0
//		}
//		let res = actualLayout.layoutAttributesForElements(in: CGRect(x: rect.origin.x, y: rect.origin.y - delta, width: rect.size.width, height: rect.size.height + delta))
//		print("- layoutAttributesForElementsInRect:\(rect)")
//		print("   -> #n# element<:s>".applyingCommonTokens(number: XibLocNumber(res?.count ?? 0)))
//		return res
//	}
	
	@objc func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
		let res = actualLayout.targetContentOffset(forProposedContentOffset: proposedContentOffset)
//		print("- targetContentOffsetForProposedContentOffset: \(proposedContentOffset)")
//		print("   -> \(res)")
		if let height = Self.height, let offset = Self.offset {
			return CGPoint(x: res.x, y: offset + (collectionViewContentSize().height - height))
		} else {
			return res
		}
	}
	
}
