/*
 * ScrollStableCollectionViewLayout.swift
 * InsertAbove
 *
 * Created by François Lamboley on 25/03/2022.
 */

import Foundation
import UIKit



class ScrollStableCollectionViewLayout : UICollectionViewLayout {
	
	static let allowHeaders = false
	static let interCell: CGFloat = 3
	static let headerHeight: CGFloat = 33
	static let estimatedCellHeight: CGFloat = 42
	
	var headersAttributes: [UICollectionViewLayoutAttributes] = []
	var itemAttributes: [[UICollectionViewLayoutAttributes]] = []
	
	var preferredHeights: [IndexPath: CGFloat] = [:]
	
	override func prepare() {
		super.prepare()
		
		guard let collectionView = collectionView, let dataSource = collectionView.dataSource else {
			return
		}
		
		itemAttributes.removeAll()
		headersAttributes.removeAll()
		
		var curY: CGFloat = 0
		for sectionIndex in 0..<(dataSource.numberOfSections?(in: collectionView) ?? 1) {
			if Self.allowHeaders {
				headersAttributes.append({
					let ret = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, with: IndexPath(item: 0, section: sectionIndex))
					ret.frame = CGRect(x: 0, y: curY, width: collectionView.bounds.width, height: Self.headerHeight)
					return ret
				}())
				curY += Self.headerHeight
			}
			
			var cells = [UICollectionViewLayoutAttributes]()
			for itemIndex in 0..<dataSource.collectionView(collectionView, numberOfItemsInSection: sectionIndex) {
				let indexPath = IndexPath(item: itemIndex, section: sectionIndex)
				cells.append({
					let ret = UICollectionViewLayoutAttributes(forCellWith: indexPath)
					ret.frame = CGRect(x: 0, y: curY, width: collectionView.bounds.width, height: preferredHeights[indexPath] ?? Self.estimatedCellHeight)
					return ret
				}())
				curY += preferredHeights[indexPath] ?? Self.estimatedCellHeight
				curY += Self.interCell
			}
			itemAttributes.append(cells)
		}
	}
	
	override func invalidateLayout() {
		print("Invalidated (no context)")
		/* AFAIU this calls `invalidateLayout(with: contextForEverythingAndCounts)`.
		 * EDIT: Nope. */
		super.invalidateLayout()
	}
	
	override func invalidateLayout(with context: UICollectionViewLayoutInvalidationContext) {
		print("Invalidated with \(context.debugDescription)")
		super.invalidateLayout(with: context)
		
		/* For now we recompute everything, it’s easier. */
		prepare()
	}
	
	override var collectionViewContentSize: CGSize {
		guard let collectionView = collectionView else {return .zero}
		return CGSize(width: collectionView.bounds.width, height: itemAttributes.last?.last?.frame.maxY ?? 0)
	}
	
	override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		guard let collectionView = collectionView, let dataSource = collectionView.dataSource else {
			return nil
		}
		
		return (headersAttributes + itemAttributes.flatMap{ $0 }).filter{ $0.frame.intersects(rect) }
	}
	
	override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		guard indexPath.section < itemAttributes.count else {return nil}
		let section = itemAttributes[indexPath.section]
		
		guard indexPath.item < section.count else {return nil}
		return section[indexPath.item]
	}
	
	override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		guard elementKind == UICollectionView.elementKindSectionHeader else {return nil}
		guard indexPath.item == 0 else {return nil}
		
		guard indexPath.section < headersAttributes.count else {return nil}
		return headersAttributes[indexPath.section]
	}
	
	override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
		return false
	}
	
	override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
		return super.invalidationContext(forBoundsChange: newBounds)
	}
	
	override func shouldInvalidateLayout(
		forPreferredLayoutAttributes preferredAttributes: UICollectionViewLayoutAttributes,
		withOriginalAttributes originalAttributes: UICollectionViewLayoutAttributes
	) -> Bool {
		return (originalAttributes.representedElementCategory == .cell && originalAttributes.frame.height != preferredAttributes.frame.height)
	}
	
	override func invalidationContext(
		forPreferredLayoutAttributes preferredAttributes: UICollectionViewLayoutAttributes,
		withOriginalAttributes originalAttributes: UICollectionViewLayoutAttributes
	) -> UICollectionViewLayoutInvalidationContext {
		switch originalAttributes.representedElementCategory {
			case .cell:
				preferredHeights[originalAttributes.indexPath] = preferredAttributes.size.height
				
			case .supplementaryView, .decorationView:
				(/*nop (unsupported, should not happen)*/)
				
			@unknown default:
				(/*nop*/)
		}
		
		let collectionView = collectionView!
		let context = super.invalidationContext(forPreferredLayoutAttributes: preferredAttributes, withOriginalAttributes: originalAttributes)
		
		let invalidatedSectionsRange = (originalAttributes.indexPath.section..<collectionView.numberOfSections)
		let invalidatedItemIndexPaths = invalidatedSectionsRange.flatMap{ sectionIndex in
			(originalAttributes.indexPath.item..<collectionView.numberOfItems(inSection: sectionIndex)).map{ itemIndex in
				IndexPath(item: itemIndex, section: sectionIndex)
			}
		}
		context.invalidateItems(at: invalidatedItemIndexPaths)
		context.invalidateSupplementaryElements(ofKind: UICollectionView.elementKindSectionHeader, at: invalidatedSectionsRange.map{ IndexPath(item: 0, section: $0) })
		
		let heightAdjustment = preferredAttributes.size.height - originalAttributes.size.height
		context.contentSizeAdjustment.height = heightAdjustment
		return context
	}
	
	override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		let ret = super.initialLayoutAttributesForAppearingItem(at: itemIndexPath)
		return ret
	}
	
	override func layoutAttributesForInteractivelyMovingItem(at indexPath: IndexPath, withTargetPosition position: CGPoint) -> UICollectionViewLayoutAttributes {
		let ret = super.layoutAttributesForInteractivelyMovingItem(at: indexPath, withTargetPosition: position)
		return ret
	}
	
}
