/*
 * TableViewController.swift
 * InsertAbove
 *
 * Created by François Lamboley on 2022/03/20.
 */

import UIKit

import ASAPExecution



class TableViewController: UITableViewController {
	
	var ds: UITableViewDiffableDataSource<Int, Int>!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		ds = UITableViewDiffableDataSource<Int, Int>(tableView: tableView, cellProvider: { tableView, indexPath, identifier in
			let cell = tableView.dequeueReusableCell(withIdentifier: "yolo", for: indexPath)
			var conf = UIListContentConfiguration.cell()
			conf.text = "yolo \(identifier)"
			cell.contentConfiguration = conf
			return cell
		})
		tableView.dataSource = ds
		
		var snap = NSDiffableDataSourceSnapshot<Int, Int>()
		snap.appendSections([0])
		snap.appendItems(Array(0..<150), toSection: 0)
		ds.apply(snap)
		
//		tableView.scrollToItem(at: IndexPath(item: 3, section: 0), at: .top, animated: false)
//		tableView.contentOffset.y = 1
		
		DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
			var s = self.ds.snapshot()
			s.insertItems(Array(-50..<0), beforeItem: 0)
			print("yo")
			
			ASAPExecution.when((!self.tableView.isDragging || self.tableView.isDecelerating) && self.tableView.contentOffset.y + self.tableView.adjustedContentInset.top >= 0, do: { _ in
				let effectiveContentOffset = self.tableView.contentOffset.y + self.tableView.adjustedContentInset.top
				print("effective offset: \(effectiveContentOffset)")
				
				if effectiveContentOffset < 1 {
					print("changing offset")
					self.tableView.contentOffset.y = -self.tableView.adjustedContentInset.top + 1
				}
				
				print("applying")
				self.ds.apply(s, animatingDifferences: true)
				let effectiveContentOffset2 = self.tableView.contentOffset.y + self.tableView.adjustedContentInset.top
				print("new effective offset after apply \(effectiveContentOffset2)")
				
				if effectiveContentOffset < 1 {
					print("reverting offset")
					self.tableView.contentOffset.y += effectiveContentOffset - 1
				}
			}, runLoopModes: [.common])
		})
	}
	
}
