/*
 * CollectionViewController.swift
 * InsertAbove
 *
 * Created by François Lamboley on 2022/03/20.
 */

import UIKit

import ASAPExecution
import STableLayout



class CollectionViewController: UIViewController, UICollectionViewDataSource, STableLayoutDelegate {
	
	@IBOutlet var collectionView: UICollectionView!
	
	var ds: UICollectionViewDiffableDataSource<Int, Int>!
	
	var cellRegistration: UICollectionView.CellRegistration<UICollectionViewCell, Int>!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		collectionView.isPrefetchingEnabled = false
		
		/* ***** LAYOUT ***** */
		
		let needsWidth: Bool
//		setupLayout1(); needsWidth = false // (Hacked) Compositional layout
//		setupLayout2(); needsWidth = true  // Standard flow layout
//		setupLayout3(); needsWidth = true  // Scroll stable layout
		setupLayout4(); needsWidth = true  // Chat layout
		
		/* ***** DATA SOURCE (providers) ***** */
		
		cellRegistration = UICollectionView.CellRegistration<UICollectionViewCell, Int>(handler: { [collectionView] cell, indexPath, identifier in
			cell.contentConfiguration = CellConfig(height: CGFloat(abs(identifier) * 3 + 21), forcedWidth: needsWidth ? (collectionView!.bounds.width - collectionView!.layoutMargins.left - collectionView!.layoutMargins.right) : nil)
			cell.backgroundConfiguration = .listPlainCell()
			cell.backgroundConfiguration?.backgroundColor = .systemTeal
		})
		let supplementaryRegistration = UICollectionView.SupplementaryRegistration<UICollectionReusableView>(elementKind: UICollectionView.elementKindSectionHeader, handler: { view, kind, indexPath in
			let label: UILabel
			if view.subviews.isEmpty {
				view.backgroundColor = .red
				label = UILabel()
				label.translatesAutoresizingMaskIntoConstraints = false
				view.addSubview(label)
				NSLayoutConstraint.activate([
					label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
					view.centerYAnchor.constraint(equalTo: label.centerYAnchor)
//					view.topAnchor.constraint(equalTo: label.topAnchor),
//					view.bottomAnchor.constraint(equalTo: label.bottomAnchor)
				])
			} else {
				label = view.subviews.first as! UILabel
			}
			let n = self.ds.numberOfSections(in: self.collectionView)
			label.text = "\(indexPath.section - (n > 151 ? 50 : 0))"
		})
		
		ds = UICollectionViewDiffableDataSource<Int, Int>(collectionView: collectionView, cellProvider: { [cellRegistration = cellRegistration!] collectionView, indexPath, identifier in
			collectionView.dequeueConfiguredReusableCell(using: cellRegistration, for: indexPath, item: identifier)
		})
		ds.supplementaryViewProvider = { collectionView, string, indexPath in
			collectionView.dequeueConfiguredReusableSupplementary(using: supplementaryRegistration, for: indexPath)
		}
		
		/* ***** DATA SOURCE (data) ***** */
		
		print("SET COLLECTION VIEW DATASOURCE")
		collectionView.dataSource = ds
		
		var snap = NSDiffableDataSourceSnapshot<Int, Int>()
//		snap.appendSections([0])
//		snap.appendItems(Array(0..<150), toSection: 0)
		snap.appendSections(Array(0..<150))
		for id in snap.sectionIdentifiers {snap.appendItems([id * 3, id * 3 + 1, id * 3 + 2], toSection: id)}
		ds.apply(snap)
		
		/* ***** INITIAL SCROLL ***** */
		
		let targetSection = 75
		if let layout = collectionView.collectionViewLayout as? STableLayout {
			let positionSnapshot = STableLayoutPositionSnapshot(indexPath: IndexPath(item: 0, section: targetSection), kind: .header, edge: .top)
			/* On iOS 14, the scroll is not immediately correct. */
			ASAPExecution.until(
				/* IMPORTANT NOTE: The condition below only works if the destination offset is far enough from current one (cell is not already visible). */
				self.collectionView.cellForItem(at: IndexPath(item: 0, section: targetSection)) != nil,
				do: { _ in
					layout.restoreContentOffset(with: positionSnapshot)
				},
				endHandler: { print("Scroll went ok: \(!$0)") },
				delay: 0.01,
				maxRunCount: 42
			)
		} else {
			ASAPExecution.until(
				{
					guard let headerItem = self.collectionView.collectionViewLayout.layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, at: IndexPath(item: 0, section: targetSection)) else {
						return false
					}
					return self.collectionView.cellForItem(at: IndexPath(item: 0, section: targetSection)).flatMap{ abs( self.collectionView.bounds.minY + self.collectionView.adjustedContentInset.top - ($0.frame.minY - headerItem.frame.height)) < 0.01 } ?? false
				}(),
				do: { _ in
					guard let headerItem = self.collectionView.collectionViewLayout.layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, at: IndexPath(item: 0, section: targetSection)),
							let  firstItem = self.collectionView.collectionViewLayout.layoutAttributesForItem(at: IndexPath(item: 0, section: targetSection))
					else {
						return
					}
					self.collectionView.contentOffset.y = -self.collectionView.adjustedContentInset.top + firstItem.frame.minY - headerItem.frame.height
				},
				endHandler: { print("Scroll went ok: \(!$0)") },
				delay: 0.01,
				maxRunCount: 42
			)
		}
#if false
		DispatchQueue.main.async{
			self.collectionView.collectionViewLayout.layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, at: IndexPath(item: 0, section: targetSection))
				.flatMap{ self.collectionView.contentOffset.y = $0.frame.minY }
			print(self.collectionView.contentOffset)
			print(self.collectionView.visibleCells.map{ ($0.contentView.subviews.first as? UILabel)?.text })
			DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)){
				self.collectionView.collectionViewLayout.layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, at: IndexPath(item: 0, section: targetSection))
					.flatMap{ self.collectionView.contentOffset.y = $0.frame.minY }
				print(self.collectionView.contentOffset)
				print(self.collectionView.visibleCells.map{ ($0.contentView.subviews.first as? UILabel)?.text })
			}
//			self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 3), at: .top, animated: false)
		}
#endif
//		collectionView.contentOffset.y = 1
		
		/* ***** ADDING CELLS ON TOP ***** */
		
		DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
			var s = self.ds.snapshot()
			let new = Array(-50..<0)
			s.insertSections(new, beforeSection: 0)
//			s.appendSections(new)
			for id in new {s.appendItems([id], toSection: id)}
			print("yo")
			
			ASAPExecution.when(true || self.collectionView.isDecelerating && self.collectionView.contentOffset.y + self.collectionView.adjustedContentInset.top >= 0, do: { _ in
				let effectiveContentOffset = self.collectionView.contentOffset.y + self.collectionView.adjustedContentInset.top
				HackedCompositionalLayout.height = self.collectionView.collectionViewLayout.collectionViewContentSize.height
				HackedCompositionalLayout.offset = self.collectionView.contentOffset.y
				print("effective offset: \(effectiveContentOffset)")
				
//				if effectiveContentOffset < 1 {
//					print("changing offset")
//					self.collectionView.contentOffset.y = -self.collectionView.adjustedContentInset.top + 1
//				}
				
				print("content size: \(self.collectionView.collectionViewLayout.collectionViewContentSize)")
				print("applying")
				self.ds.apply(s, animatingDifferences: true)
				print("content size after apply: \(self.collectionView.collectionViewLayout.collectionViewContentSize)")
				let effectiveContentOffset2 = self.collectionView.contentOffset.y + self.collectionView.adjustedContentInset.top
				print("new effective offset after apply \(effectiveContentOffset2)")
				
//				if effectiveContentOffset < 1 {
//					print("reverting offset")
//					self.collectionView.contentOffset.y += effectiveContentOffset - 1
//				}
			}, retryDelay: 0.01, runLoopModes: [.common])
		})
	}
	
	private func setupLayout1() {
//		let group = NSCollectionLayoutGroup.vertical(
//			layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .estimated(44)),
//			subitems: [
//				NSCollectionLayoutItem.init(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1)))
//			]
//		)
//		let listSectionLayout: NSCollectionLayoutSection = {
//			let res = NSCollectionLayoutSection(group: group)
//			res.boundarySupplementaryItems = {
//				let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(20))
//				let headerElement = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
//				headerElement.pinToVisibleBounds = true
//				return [headerElement]
//			}()
//			return res
//		}()
//		let layout: UICollectionViewCompositionalLayout = {
//			let res = UICollectionViewCompositionalLayout(section: listSectionLayout)
//			res.configuration = {
//				let res = UICollectionViewCompositionalLayoutConfiguration()
//				res.interSectionSpacing = 0
//				return res
//			}()
//			return res
//		}()
		
		let config: UICollectionLayoutListConfiguration = {
			var res = UICollectionLayoutListConfiguration(appearance: .plain)
			res.headerMode = .supplementary
			res.footerMode = .none
			res.showsSeparators = false
			if #available(iOS 15.0, *) {
				res.headerTopPadding = 0
			}
			return res
		}()
		let layout: UICollectionViewCompositionalLayout = {
			let res = UICollectionViewCompositionalLayout.list(using: config)
			res.configuration.boundarySupplementaryItems = {
				let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(75))
				let headerElement = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
				headerElement.pinToVisibleBounds = true
				return [headerElement]
			}()
			return res
		}()
		collectionView.collectionViewLayout = unsafeBitCast(HackedCompositionalLayout(actualLayout: layout), to: UICollectionViewCompositionalLayout.self)
	}
	
	private func setupLayout2() {
		collectionView.collectionViewLayout = {
			let res = UICollectionViewFlowLayout()
			res.sectionHeadersPinToVisibleBounds = true
			res.estimatedItemSize = CGSize(width: 42, height: 75)
			res.headerReferenceSize = CGSize(width: 42, height: 33)
			return res
		}()
	}
	
	private func setupLayout3() {
		collectionView.collectionViewLayout = {
			let res = ScrollStableCollectionViewLayout()
			return res
		}()
	}
	
	private func setupLayout4() {
		collectionView.collectionViewLayout = {
			let res = STableLayout()
			res.delegate = self
			res.settings.allowPinning = true
			res.settings.interItemSpacing = 3
			res.settings.interSectionSpacing = 3
			res.keepContentOffsetAtBottomOnBatchUpdates = true
			return res
		}()
	}
	
	/* ********************************
	   MARK: - CollectionViewDataSource
	   ******************************** */
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 150
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		return collectionView.dequeueConfiguredReusableCell(using: cellRegistration, for: indexPath, item: indexPath.item)
	}
	
	/* **************************
	   MARK: - ChatLayoutDelegate
	   ************************** */
	
	func shouldPresentHeader(_ chatLayout: STableLayout, at sectionIndex: Int) -> Bool {
		return true
	}
	
	func pinningForItem(_ chatLayout: STableLayout, of kind: ItemKind, at indexPath: IndexPath) -> STableItemPinning {
		return kind == .header ? .top : .none
	}
	
}
