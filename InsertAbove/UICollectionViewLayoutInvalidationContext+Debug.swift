/*
 * UICollectionViewLayoutInvalidationContext+Debug.swift
 * InsertAbove
 *
 * Created by François Lamboley on 2022/03/28.
 * Initially created by Bryan Keller for Airbnb on 2018/02/27.
 */

import UIKit



extension UICollectionViewLayoutInvalidationContext {
	
	open override var debugDescription: String {
		var values = [String]()
		
		if invalidateEverything {
			values.append("everything")
		}
		
		if invalidateDataSourceCounts {
			values.append("counts")
		}
		
		if contentOffsetAdjustment != .zero {
			values.append("offset = \(contentOffsetAdjustment)")
		}
		
		if contentSizeAdjustment != .zero {
			values.append("size = \(contentSizeAdjustment)")
		}
		
		if let indexPaths = invalidatedItemIndexPaths, !indexPaths.isEmpty {
			values.append("items = \(summaryOfIndexPaths(indexPaths))")
		}
		
		if let indexPathsByKind = invalidatedSupplementaryIndexPaths, !indexPathsByKind.isEmpty {
			values.append(summaryOfIndexPathsByKind(indexPathsByKind))
		}
		
		if let indexPathsByKind = invalidatedDecorationIndexPaths, !indexPathsByKind.isEmpty {
			values.append(summaryOfIndexPathsByKind(indexPathsByKind))
		}
		
		return "{\(values.joined(separator: ", "))}"
	}
	
	private func summaryOfIndexPathsByKind(_ indexPathsByKind: [String: [IndexPath]]) -> String {
		var values = [String]()
		for (kind, indexPaths) in indexPathsByKind {
			values.append("\(kind) = \(summaryOfIndexPaths(indexPaths))")
		}
		return values.joined(separator: ", ")
	}
	
	private func summaryOfIndexPaths(_ indexPaths: [IndexPath]) -> String {
		return "[\(indexPaths.sorted().map{ "{\($0.section), \($0.item)}" }.joined(separator: ", "))]"
	}
	
}
