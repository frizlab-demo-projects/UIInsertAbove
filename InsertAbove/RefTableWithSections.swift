/*
 * RefTableWithSections.swift
 * InsertAbove
 *
 * Created by François Lamboley on 31/03/2022.
 */

import Foundation
import UIKit



class RefTableWithSections : UITableViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if #available(iOS 15.0, *) {
			tableView.sectionHeaderTopPadding = 0
		}
	}
	
}
