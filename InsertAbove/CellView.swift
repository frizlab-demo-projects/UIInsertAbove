/*
 * CellView.swift
 * InsertAbove
 *
 * Created by François Lamboley on 2022/03/22.
 */

import Foundation
import UIKit



class CellView : UIView, UIContentView {
	
	var configuration: UIContentConfiguration {
		didSet {
			let config = configuration as! CellConfig
			label.text = "\(config.height)"
//			constraintHeight.constant = config.height
			invalidateIntrinsicContentSize()
		}
	}
	
	let label: UILabel
//	var constraintHeight: NSLayoutConstraint
	
	init(config: CellConfig) {
		self.configuration = config
		
		self.label = UILabel()
		self.label.translatesAutoresizingMaskIntoConstraints = false
//		self.constraintHeight = label.heightAnchor.constraint(equalToConstant: 0)
//		self.constraintHeight.priority = .defaultHigh
		
		super.init(frame: .zero)
		
		addSubview(label)
		label.setContentHuggingPriority(.required, for: .vertical)
		label.setContentCompressionResistancePriority(.required, for: .vertical)
		
		NSLayoutConstraint.activate([
//			constraintHeight,
//			label.topAnchor.constraint(equalTo: topAnchor),
//			label.bottomAnchor.constraint(equalTo: bottomAnchor),
//			label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
			label.centerYAnchor.constraint(equalTo: centerYAnchor),
			
			label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8)
		])
	}
	
	required init?(coder: NSCoder) {
		return nil
	}
	
	/* Could be fully removed if using the (commented) fully-constrained alternative.
	 * The commented constrained alternative is not currently equivalent to that one: the width would be incorrect (no constraint to width). */
	override var intrinsicContentSize: CGSize {
		let config = (configuration as! CellConfig)
		return CGSize(width: config.forcedWidth ?? UIView.noIntrinsicMetric, height: config.height)
	}
	
}


struct CellConfig : UIContentConfiguration {
	
	var height: CGFloat = 20
	var forcedWidth: CGFloat?
	
	func makeContentView() -> UIView & UIContentView {
		return CellView(config: self)
	}
	
	func updated(for state: UIConfigurationState) -> CellConfig {
		return self
	}
	
}
